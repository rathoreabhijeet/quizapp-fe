angular.module('starter')

    .controller('checkAnswerCtrl', checkAnswerCtrl);
function checkAnswerCtrl(currentQuiz,$state,$stateParams){
    var checkAnswer = this;
    //getting params form quiz details
    checkAnswer.quizDetailKey=$stateParams.key;
    checkAnswer.previousAttempt=$stateParams.history;

      //filter check for filtered by  history and filtered Questions
    if(checkAnswer.quizDetailKey==null){
        checkAnswer.list=currentQuiz.filteredQuestions;

    }
    else{
        checkAnswer.list=checkAnswer.previousAttempt.result;

    }
    checkAnswer.listLength=checkAnswer.list.length;



    var count=0;
    checkAnswer.check=function(ans){
        return Array.isArray(ans);

    }
    checkAnswer.matchTrue=null;
    //check for multiple selected answer is match with correct or not

    checkAnswer.match=function(optionId,correctArray){
        if(Array.isArray(correctArray)&&(correctArray.indexOf(optionId)>=0)){

            return checkAnswer.matchTrue=true;
        }

            else
            return checkAnswer.matchTrue=false;


    }
      checkAnswer.matchArray=function(answer,correct){
          if(JSON.stringify(answer)==JSON.stringify(correct)){

              return true;

          }
          else{

              return false;
          }

      }

    checkAnswer.Goback=function(){
        if(checkAnswer.quizDetailKey!=null){
            $state.go(checkAnswer.quizDetailKey);
        }
        else{
            $state.go('results');
        }

    }


  checkAnswer.GoTohome = function()
  {
    $state.go('home');
  }

}

