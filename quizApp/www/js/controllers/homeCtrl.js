angular.module('starter')

.controller('homeCtrl', homeCtrl);
function homeCtrl(papers,topics,backButtonForHome, $state,questions){
  var home = this;

  home.papers = papers;
  home.topics = topics;

    backButtonForHome.backcallfun();

  home.icons = ['ion-gear-b','ion-crop','ion-briefcase'];

  home.goToQuiz= function(topic){

    $state.go('quizDetail',{topic:topic.id});
  }
 //topic questions count to show

    home.topicsQuestionsLength=[];
    home.topicsQuestionsLength.length=topics.length;

for(var i=0;i<=home.topicsQuestionsLength.length-1;i++){
    count=0;
     _.filter(questions,function(question){
       if(question.category.indexOf(topics[i].id)!=-1) {
           count++;
       }

    } );
    home.topicsQuestionsLength[i]=count;
}


    //paper questions count to show
    home.papersQuestionsLengthArray=[];
    home.papersQuestionsLengthArray.length=papers.length;

    for(var i=0;i<=home.papersQuestionsLengthArray.length-1;i++){
        count=0;
        _.filter(questions,function(question){
            if(question.paperId==papers[i].id) {
                count++;
            }

        } );
        home.papersQuestionsLengthArray[i]=count;
    }

}

