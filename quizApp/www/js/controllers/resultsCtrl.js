angular.module('starter')

.controller('resultsCtrl', resultsCtrl);
function resultsCtrl(currentQuiz, $localForage, $state,$stateParams){
    var resultsCtrl=this;

    console.log($stateParams.status);
    var results = this;
    var total =0;
    var attempted =0;
    var correct = 0;
    var correctMark =1;
    var negativeMark=0;
    var percentage=0;
    var resultsKey='results';
    var allResults=[];
    results.q=currentQuiz.filteredQuestions;
    console.log(results.q);
    if(results.q){
        total=results.q.length;
        _.forEach(results.q,function (v, k) {
            if(v.answer){
                attempted++;
                if(v.answer.length>=1) {

                    v.correct.sort();
                    v.answer.sort();
                    if(_.isEqual(v.answer, v.correct)){
                        console.log(v);
                        correct++;
                    }
                }
                else
                {
                    if (v.answer == v.correct) {
                        console.log(v);
                        correct++;
                    }
                }

            }
        });
        percentage=Math.floor((correct/total)*100);
        console.log("attempted", attempted, "Correct",correct, "Wrong",attempted-correct, "total", total,"percentage",percentage);
    }
    var score = (correct*correctMark-(attempted-correct)*negativeMark);
    results.result={
        total: total,
        attempted: attempted,
        correct: correct,
        score: score,
        percentage: percentage
    };

if($stateParams.status==1){
    $localForage.getItem(resultsKey).then(function (data) {
        console.log("got", data);
        if (data) {
            allResults = data;
            allResults.push({
                result: results.result,
                time: new Date(),
                paper: "paper2"
            });
            // console.log(allResults);
            $localForage.setItem(resultsKey, allResults);
        }
        else {
            allResults.push({
                result: results.result,
                time: new Date(),
                paper: "paper1"
            });
            $localForage.setItem(resultsKey, allResults);
        }
    });

    var historyKey="history";
    var allHistory=[];

    $localForage.getItem(historyKey).then(function (data) {
        console.log("got", data);
        if (data) {
            allHistory = data;
            allHistory.push({
                result:currentQuiz.filteredQuestions,
                time: new Date(),
                paper: currentQuiz.paper,
                type: currentQuiz.type,
                percentage:  results.result.percentage
            });
            // console.log(allResults);
            $localForage.setItem(historyKey, allHistory);
        }
        else {
            allHistory.push({
                result:currentQuiz.filteredQuestions,
                time: new Date(),
                paper: currentQuiz.paper,
                type: currentQuiz.type,
                percentage:  results.result.percentage
            });
            $localForage.setItem(historyKey, allHistory);
        }
    });
}
    console.log(results.result);
    currentQuiz.result=results.result;

    results.GoTocheckAnswer = function () {
        $state.go('checkAnswer');
    }
    results.GoTohome= function () {
        currentQuiz.filteredQuestions=null;
        currentQuiz.type=null;
        currentQuiz.paper=null;
        console.log("all become null");


        console.log(currentQuiz.filteredQuestions);

        $state.go('home');
    }
    console.log(currentQuiz.filteredQuestions);
    console.log(results);


}