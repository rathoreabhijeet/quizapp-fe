
angular.module('starter')
  .filter('capitalize', function () {
    return function (input, all) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : '';
    }
  })
.factory('papers',function(){
  return [{"id":1,"category":'paper1'},
    {"id":2,"category":'paper2'},
    {"id":3,"category":'paper3'}]
})

  .factory('topics',function(){
    return [{"id":1,"category":'Virtual Private Cloud', "img": "img/vpc.png"},
      {"id":2,"category":'Simple Storage Service',"img": "img/s3.png"},
      {"id":3,"category":'Simple Queue Service',"img": "img/sqs.png"},
      {"id":4,"category":'Relational Database Service',"img": "img/rds.png"},
      {"id":5,"category":'Elastic Compute Cloud',"img": "img/ec2.png"}]
  })

  .factory('questions',function(){
    return [
        {
            "id":1,
            "question":'When a snapshot is being taken against an EBS volume, the volume becomes unavailable and the instance no longer has the ability to communicate with the EBS volume until the snapshot is complete. ',
            //code:`function myFunction(a, b) {
            //  return a * b;
            //    }
            //myFunction(4, 3);`,
            "options":[{"id":1,"option":'True'},{"id":2,"option":'False'}],
            "correct":2,
            "category":[],
            "paperId":1,
            "explanation":"Volume remains available and instance keeps the ability to communicate with volume."
        },
        {
            "id":2,
            "question":"You are testing an application that uses EC2 instances to poll an SQS queue. At this stage of testing, you have verified that the EC2 instances can retrieve messages from the queue, but your coworkers are complaining about not being able to manually retrieve any messages from the queue from their on-premises workstations. What is the most likely source of this problem?",

            "options":[{"id":1,
                "option":'SQS queues only accept traffic from within AWS'},
                {"id":2,"option":'Your coworkers may not have permission to the SQS queue'},
                {"id":3,"option":'Manual polling of SQS queues is not possible'},
                {"id":4,"option":'They are using short polling'}],
            "correct":2,
            "paperId":1,
            "explanation":"Short polling may fail to retrieve messages sometimes, but if no messages can be retrieved after multiple attempts, permissions are the more likely cause.",
            "category":[]
        },
        {
            "id":3,
            "question":'Your supervisor is upset that a client’s purchase from your website was processed twice. You find out that the order process involves EC2 instances processing messages from an SQS queue. What action would you recommend to ensure this does not happen again?',
            "options":[{"id":1,"option":'Increase the visibility timeout for the queue'},
                {"id":2,"option":'Use long polling rather than short polling'},
                {"id":3,"option":'Insert code into the application to delete messages after processing'},
                {"id":4,"option":'Modify the order process to use SWF'}],
            "correct":4,
            "paperId":1,
            "category":[],
            "explanation":"The only way to ensure that a duplicate will not occur in a process is to use Amazon SWF instead of SQS. The other options can decrease the chances of duplicates in SQS, but will not eliminate them entirely."
        },
        {
            "id":4,
            "question":'You and a colleague create an SQS queue and create several messages in it. You both test your ability to manually poll the queue by using the command-line API calls. After testing, you find that your colleague’s polling attempt retrieved messages 1, 3, and 5. Your polling attempt retrieved messages 4, 6, and 8. Nether of your attempts retrieved messages 2 or 7. What is a possible cause for this behavior?',
            "options":[{"id":1,"option":'When manually polling the queue, you can only retrieve messages that you created'},
                {"id":2,"option":'SQS security group rules are prohibiting you and your colleague from retrieving messages from SQS servers in different subnets from your client computers'},
                {"id":3,"option":'You and your colleague did not see the same messages because of the visibility timeout'},
                {"id":4,"option":'You and your colleague used short polling'}],
            "correct":[3,4],
            "paperId":1,
            "category":[],
            "explanation":": When a message is retrieved, that message is hidden from other polling attempts until the message is deleted or the visibility timeout expires. Short polling does not query all the servers that the SQS messages can reside on, so multiple queries of the queue may be needed to retrieve all messages in the queue."
        },
        {
            "id":5,
            "question":"You have been told that you need to set up a bastion host by your manager in the cheapest, most secure way, and that you should be the only person that can access it via SSH. Which of the following setups would satisfy your manager's request?",
            "options":[{"id":1,"option":'A large EC2 instance and a security group which only allows access on port 22'},
                {"id":2,"option":'A large EC2 instance and a security group which only allows access on port 22 via your IP address'},
                {"id":3,"option":'A small EC2 instance and a security group which only allows access on port 22 via your IP address'},
                {"id":4,"option":'A small EC2 instance and a security group which only allows access on port 22'}],
            "correct":3,
            "paperId":1,
            "category":[],
            "explanation":"A small EC2 instance will serve well for a bastion host. Selecting large EC2 instance for this purpose will only increase the cost."
        },
        {
            "id":6,
            "question":'After building out a new VPC from scratch, you have attached an Internet gateway to the only route table within your VPC. This means all instances currently belong to a public subnet. While trying to connect to one of the instances over the Internet gateway, you are experiencing connectivity issues preventing any communication from your machine to the instance over the Internet. After investigating, you notice that the security group has proper permissions and the route table is currently as follows: 10.0.0.0/16 local 0.0.0.0/16 igw-xxxx58d8 What might be the issue?',
            "options":[{"id":1,"option":'Change the route table Destination for the IGW to be 0.0.0.0/0'},
                {"id":2,"option":'The local route needs to be updated to 0.0.0.0/0'},
                {"id":3,"option":'Change the route table Destination for the IGW to be 10.0.0.0/16'},
                {"id":4,"option":'The Internet gateway is not attached to the VPC'}],
            "correct":1,
            "paperId":1,
            "category":[],
            "explanation":"Routes in Routing table for VPC are needed to be correctly configured to prohibit connectivity problems."
        },
        {
            "id":7,
            "question":'You are setting up a VPC peering connection with another VPC. This is the first time that you have done this, and you are not that familiar with the limitations and rules. When reading up on this, you discover that there seems to be a lot of limitations and rules when it comes to VPC peering. Which of the following is NOT one of those limitations or rules?',
            //code:`function myFunction(a, b) {
            //return a * b;
            //   }
            //myFunction(4, 3);`,
            "options":[{"id":1,"option":'A placement group cannot span peered VPCs'},
                {"id":2,"option":'You cannot create a VPC peering connection between VPCs that have matching or overlapping CIDR blocks'},
                {"id":3,"option":'You cannot have more than one VPC peering connection between the same two VPCs at the same time'},
                {"id":4,"option":'You cannot create a VPC peering connection between VPCs in different regions'}],
            "correct":1,
            "paperId":1,
            "category":[],
            "explanation":"A placement group can span peered VPCs, there is no such limitation to it."
        },
        {
            "id":8,
            "question":'You create an SQS queue and decide to test it out by creating a simple application which looks for messages in the queue. When a message is retrieved, the application is supposed to delete the message. You create three test messages in your SQS queue and discover that messages 1 and 3 are quickly deleted but message 2 remains in the queue. What is a possible cause for this behavior?',
            "options":[{"id":1,"option":'The order that messages are received in is not guaranteed in SQS'},
                {"id":2,"option":'Message 2 uses JSON formatting'},
                {"id":3,"option":'Your application is using short polling'},
                {"id":4,"option":'You failed to set the correct permissions on message 2'}],
            "correct":[1,3],
            "paperId":1,
            "category":[],
            "explanation":"Short polling does not query all the servers that the SQS messages can reside on, so multiple queries of the queue may be needed to retrieve all messages in the queue."
        },
        {
            "id":9,
            "question":'Your web application front end consists of multiple EC2 instances behind an Elastic Load Balancer. You configured the ELB to perform health checks on these EC2 instances. If an instance fails to pass health checks, which statement is true?',
            "options":[{"id":1,"option":'The ELB stops sending traffic to the instance that failed its health check.'},
                {"id":2,"option":'The instance is replaced automatically by the ELB'},
                {"id":3,"option":'The instance gets quarantined by the ELB for root cause analysis'},
                {"id":4,"option":'The instance gets terminated automatically by the ELB'}],
            "correct":1,
            "paperId":1,
            "category":[],
            "explanation":"ELB checks whether all the instances are healthy or not by performing health checks, if an instance fails the health check, ELB de-register it for sending traffic to it."
        },
      {
        "id":10,
        "question":'Your company has an application that requires access to a NoSQL database. Your IT department has no desire to manage the NoSQL servers. Which Amazon service provides a fully-managed and highly available NoSQL service?',
        "options":[{"id":1,"option":'ElasticMap Reduce'},
                   {"id":2,"option":'Amazon RDS'},
                    {"id":3,"option":'DynamoDB'},
                    {"id":4,"option":'SimpleDB'}],
        "correct":3,
        "paperId":1,
        "category":[],
          "explanation":"DynamoDB is the fully-managed and highly-available NoSQL service available by AWS."
      },
      {
        "id":11,
        "question":'As part of your application architecture requirements, the company you are working for has requested the ability to run analytics against all combined log files from the Elastic Load Balancer. Which services are used together to collect logs and process log file analysis in an AWS environment?',
        "options":[ {"id":1,"option":'Amazon DynamoDB to store the logs and EC2 for running custom log analysis scripts'},
                    {"id":2,"option":'Amazon EC2 for storing and processing the log files'},
                    {"id":3,"option":'Amazon S3 for storing the ELB log files and EC2 for processing the log files in analysis'},
                    {"id":4,"option":'Amazon S3 for storing ELB log files and Amazon EMR for processing the log files in analysis'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"EC2 is not mainly for such actions. Amazon EMR is the service designed for performing such actions, it will perform analytics over the logs stored in S3."
      },

      {
        "id":12,
        "question":'Your supervisor asks you to create a decoupled application whose process includes dependencies on EC2 instances and servers located in your company’s on-premises datacenter. Which of these are you least likely to recommend as part of that process?',
        "options": [{"id":1,"option":'SQS polling from an EC2 instance deployed with an IAM role'},
                    {"id":2,"option":'An SWF workflow'},
                    {"id":3,"option":'SQS polling from an EC2 instance using IAM user credentials'},
                    {"id":4,"option":'SQS polling from an on-premises server using IAM user credentials'}],
        "correct":3,
        "paperId":1,
        "category":[],
          "explanation":"An EC2 IAM role should be used when deploying EC2 instances to grant permissions rather than storing IAM user credentials in EC2 instances."
      },

      {
        "id":13,
        "question":'You are asked to perform a security audit on a company’s AWS environment. You log in to their AWS account with the root user credentials and discover that they are using a VPN to connect to and manage their private EC2 instances. Upon further inspection, you find that they are not regularly patching their RDS instances. Finally, you notice that they are using IAM policies rather than bucket policies to manage access to their S3 buckets. What do you cite as the most critical security risk in your report?',
        "options":[ {"id":1,"option":'The company’s employees are not using a bastion host to connect to their private EC2 instances'},
                    {"id":2,"option":'The company is not using bucket policies to manage S3 bucket access'},
                    {"id":3,"option":'The company has not been patching their RDS instances'},
                    {"id":4,"option":'The company allows people to log in with their AWS account’s root user'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"A bastion host is not more secure than a VPN as a means of connecting to private instances. IAM policies and S3 bucket policies are both acceptable means of controlling S3 bucket access. It is AWS's responsibility to patch RDS instances. After initial account setup, AWS account administration should be performed by IAM users rather than the root user account."
      },

      {
        "id":14,
        "question":'Your company requires that all the data on your EBS-backed EC2 volumes be encrypted. How would you go about doing this?',
        "options":[ {"id":1,"option":'You cannot enable EBS encryption on a specific volume'},
                    {"id":2,"option":'AWS allows you to encrypt the file system on an EBS volume on EBS volume setup'},
                    {"id":3,"option":'Encryption can be done on the OS layer of the EBS volume'},
                    {"id":4,"option":'None of the above'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"Flexibility in setting up encryption over your data is provided by AWS. You can easily manage encryption of all of your data."
      },
      {
        "id":15,
        "question":'One of your more important clients is a Telecom business who needs to process some real-time data in a distributed manner. They suggest to you that they think they should use either Amazon SQS or Amazon Kinesis to achieve this and they want you to tell them what would be the difference between the two. After some research you decide that they should use Kinesis and are trying to put together some reasons for this. One of the below statements is INCORRECT, regarding this. Which one?',
        "options":[ {"id":1,"option":'Amazon SQS, because of its distributed nature does not, despite what the name suggests, guarantee FIFO(First in First Out). Kinesis can guarantee FIFO'},
            {"id":2,"option":'Kinesis cannot route related data records to the same record processor (as in streaming MapReduce).'},
            {"id":3,"option":'Kinesis has the ability for multiple applications to consume the same stream concurrently'},
            {"id":4,"option":'Kinesis has the ability to consume data records in the same order a few hours later'}],
        "correct":2,
        "paperId":1,
        "category":[],
        "explanation":"Kinesis is a powerful AWS service and you can easily route related data records to the same record processor using Kinesis."
      },

      {
        "id":16,
        "question":"You're building out an application on AWS that is running within a single region. However, you're designing with disaster recovery in mind. Your intention is to build the application so that if the current region becomes unavailable, you can failover to another region. Part of your application relies heavily on pre-built AMIs. In order to share those AMIs with the region you're using as a backup, what process would you take?",
        "options":[ {"id":1,"option":"Nothing, because all AMI's are available in any region as long as it is created within the same account"},
                    {"id":2,"option":'Modify the image permissions to share to the designated backup region'},
                    {"id":3,"option":'Copy the AMI from the current region to another region, modify the Auto Scaling groups in the backup region to use the new AMI ID in the backup region'},
                    {"id":4,"option":'Modify the image permissions to share the AMI with another account, then set the default region to the backup region'}],
        "correct":3,
        "paperId":1,
        "category":[],
          "explanation":"AMIs are region specific i-e AMI is available within the region in which it was created. For using it in other regions you have to copy it in that region."
      },

      {
        "id":17,
        "question":'You design an application that checks for new items in an S3 bucket once per hour. If new items exist, a message is added to an SQS queue. You have several EC2 instances which retrieve messages from the SQS queue, parse the file, and send you an email containing the relevant information from the file. You upload one test file to the bucket, wait a couple hours and find that you have hundreds of emails from the application. What is the most likely cause for this volume of email?',
        "options":[ {"id":1,"option":'Your application does not issue a delete command to the SQS queue after processing the message'},
                    {"id":2,"option":'You can only have one EC2 instance polling the SQS queue at a time'},
                    {"id":3,"option":'This is expected behavior when using short polling because SQS does not guarantee that there will not be duplicate messages processed'},
                    {"id":4,"option":'This is expected behavior when using long polling because SQS does not guarantee that there will not be duplicate messages processed'}],
        "correct":1,
        "paperId":1,
        "category":[],
          "explanation":"Many instances can poll a single queue, but to keep multiple instances from processing the same SQS message, your application must delete the SQS message after processing it. While SQS does not guarantee a message will be processed only once, the same message being processed many times is probably an indication that the message is not being deleted following processing."
      },
      {
        "id":18,
        "question":'You own an image manipulation application. Your users take a picture, upload it to your app, and request filters to be added to the image. You need to decouple the application so your users are not waiting for the image processing to take place. How would you go about doing this?',
        "options":[ {"id":1,"option":'Use Amazon S3 to store the images and Amazon EC2 to process the request'},
                    {"id":2,"option":'Use Amazon SQS to store the requests using metadata and JSON in the message, use S3 to store the image, and Auto Scaling to determine when to fire off more worker instances based on queue size'},
                    {"id":3,"option":'Integrate with DynamoDB to allow messages to be sent back and forth between our worker instances and EC2 instances'},
                    {"id":4,"option":'Use Amazon SNS to process image requests'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"Autoscaling will help us by removing and adding worker instances for corresponding scale-out and scale-in events and will help in reducing the cost."
      },
      {
        "id":19,
        "question":'Your supervisor asks you to create a decoupled application using EC2 instances polling an SQS queue. Which of the following is not necessary to accomplish this goal?',
        "options": [{"id":1,"option":'Application code which polls for messages in an SQS queue'},
                    {"id":2,"option":'An Elastic Load Balancer to distribute traffic from EC2 instances to the SQS queue'},
                    {"id":3,"option":'An IAM role allowing EC2 instances to modify an SQS queue'},
                    {"id":4,"option":'Multiple EC2 application server instances'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"While an Elastic Load Balancer is often used to handle incoming web traffic requests in a highly available web application solution, it is not required for a decoupled application's EC2 instances to poll SQS. A decoupled application requires that one component of the application can fail without the entire environment failing. Using multiple EC2 instances that have been launched with a role granting SQS queue permissions and using code to poll an SQS queue allows an EC2 instance to fail, even if it has already begun processing a job because another instance is able to process the job when the message is returned to the queue."
      },
      {
          "id":20,
          "question":'Your company is posting a big article on the front page of your website tomorrow. It is expected that the demand could potentially overwhelm your infrastructure. In the event of a load failure, how can you set up DNS failover to a static website?',
          "options":[ {"id":1,"option":'Build out additional capacity to ensure there is no scenario in which the application can fail'},
              {"id":2,"option":'Use Route 53 and the failover option to failover to a static S3 website bucket or CloudFront distribution in the event of an issue'},
              {"id":3,"option":'Duplicate the exact application architecture in another region and configure DNS latency-based routing'},
              {"id":4,"option":'Enable failover to an on-premise data center'}],
          "correct":2,
          "paperId":1,
          "category":[],
          "explanation":"In case of load-failure, Roiute53 failover option will point traffic to S3 static website or CloudFront distribution to ensure zero downtime."
      },
      {
        "id":21,
        "question":'Your EC2 instances are configured to run behind an Amazon VPC. You have assigned two web serves instances to an Elastic Load Balancer. However, the instances and the ELB are not reachable via URL to the elastic load balancer serving the web app data from the EC2 instances. How might you resolve the issue so that your instances are serving the web app data to the public Internet?',
        "options":[ {"id":1,"option":'Attach an Internet gateway to the VPC and route it to the subnet'},
                    {"id":2,"option":'Add an elastic IP address to the instance'},
                    {"id":3,"option":'Use Amazon Elastic Load Balancer to serve requests to your instances located in the internal subnet'},
                    {"id":4,"option":'None of the above'}],
        "correct":1,
        "paperId":1,
        "category":[2,4],
          "explanation":"Setting up an internet gateway is essential for outside connectivity."
      },
      {
        "id":22,
        "question":'You manage a web application on EC2 instances. Incoming requests are saved as messages in an SQS queue with the maximum message retention period. You return from a week and a half vacation to find that the part of the application which processes received requests is hung and the queue has grown to 1,000 messages. While you are working on resolving the application issue, you need to post an update to end users. What information should that update include?',
        "options":[ {"id":1,"option":'An apology for the delay in processing requests , assurance that the application will be operational shortly, and a note that all requests made in the last ten days will need to be resubmitted (because the portion of the application that processes received requests was down)'},
                    {"id":2,"option":'An apology for the delay in processing requests, assurance that the application will be operational shortly, and a note that all received requests will be processed at that time'},
                    {"id":3,"option":'An apology for the delay in processing requests, assurance that the application will be operational shortly, and a note that recently received requests will need to be re-submitted (as the SQS queue has reached the maximum of 1,000 messages, meaning recently submitted requests could not be saved as SQS messages)'},
                    {"id":4,"option":'An apology for the delay in processing requests, assurance that the application will be operational shortly, and a note that requests greater than ten days old will need to be resubmitted (as they have surpassed the SQS queue’s retention period)'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"Use of an SQS queue to decouple the application means that a virtually unlimited number of received requests can be stored in the SQS queue for a maximum of 14 days and can be processed when the portion of the application that is responsible for processing received requests becomes operational again."
      },
      {
        "id":23,
        "question":'After building an application that makes use of an Elastic Load Balancer over port 80, you notice that your instances, even though they are healthy as part of the health check, are not serving traffic when you go to the ELB DNS cname. What might be the cause of this issue?',
        "options":[ {"id":1,"option":'The ELB security group does not have port 80 open'},
                    {"id":2,"option":'There is no attached Internet gateway on the private subnet'},
                    {"id":3,"option":'The EC2 instances do not have port 80 open'},
                    {"id":4,"option":'The EC2 instances are part of a public subnet'}],
        "correct":[1,3],
        "paperId":1,
        "category":[],
          "explanation":"Remember that health checks do not have to be performed on port 80. This is the default, yes, but it can be changed. So in this case we could have a health check configured on a different port which is why it would be marked as healthy even though the ELB security group and/or the EC2 instance security group on port 80 could be closed. Even if this scenario does not sound very likely, it is possible. AWS likes to try and trick test takers by throwing in scenarios like these, which is why we included this tricky question."
      },
      {
        "id":24,
        "question":"The KPL is an easy-to-use, highly configurable library that helps you write to an Amazon Kinesis stream. It acts as an intermediary between your producer application code and the stream's API actions. One of its key concepts is aggregation. Which of the following best describes aggregation as it relates to the KPL?",
        "options":[ {"id":1,"option":'It is a blob of data that has particular meaning to the user. Examples include a JSON blob representing a UI event on a web site, or a log entry from a web server'},
                    {"id":2,"option":"It refers to batching multiple streams' records and sending them in a single HTTP request with a call to the API operation PutRecords, instead of sending each stream's record in its own HTTP request"},
                    {"id":3,"option":'It refers to performing a single action on multiple items instead of repeatedly performing the action on each individual item'},
                    {"id":4,"option":"It refers to the storage of multiple records in a stream's record and allows customers to increase the number of records sent per API call, which effectively increases producer throughput"}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"AWS Kenisis is responsible for creation and maintaining large data streams. It allows users to increase the number of records sent per API call."
      },
      {
        "id":25,
        "question":'You are working for a startup company that is building an application that receives large amounts of data. Unfortunately, current funding has left the start-up short on cash, cannot afford to purchase thousands of dollars of storage hardware, and has opted to use AWS. Which services would you implement in order to store a virtually unlimited amount of data without any effort to scale when demand unexpectedly increases?',
        "options":[ {"id":1,"option":'Amazon EC2, because EBS volumes can scale to hold any amount of data and, when used with Auto Scaling, can be designed for fault tolerance and high availability'},
                    {"id":2,"option":'Amazon Glacier, to keep costs low for storage and scale infinitely'},
                    {"id":3,"option":'Amazon Import/Export, because Amazon assists in migrating large amounts of data to Amazon S3'},
                    {"id":4,"option":'Amazon S3, because it provides unlimited amounts of storage data, scales automatically, is highly available, and durable'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"You cannot afford purchasing physical storage servers and high scalability, durability are your main concerns which comes with S3."
      },
      {
        "id":26,
        "question":'When designing an application architecture utilizing EC2 instances and the ELB, to determine the instance size required for your application what questions might be important?',
        "options":[ {"id":1,"option":'Determine the required I/O operations'},
                    {"id":2,"option":'Determining the minimum memory requirements for an application'},
                    {"id":3,"option":'Determining the peek expected usage for a clients application'},
                    {"id":4,"option":'Determining where the client intends to serve most of the traffic'}],
        "correct":[1,2],
        "paperId":1,
        "category":[],
          "explanation":"Determining the required I/O and minimum memory requirements are the main concerns to avoid downtime and provisioning a highly available infrastructure."
      },
      {
        "id":27,
        "question":"Your company is building it's first deployment on AWS and is concerned about security in the cloud. There will be a lot of online payment transactions happening once the infrastructure is deployed. Your security officer has come to you and wants to know whether they should be using the Hardware Security Module (HSM) or AWS Key Management Service (KMS) for encryption. Which of the following would be the most correct response to give him?",
        "options":[ {"id":1,"option":'AWS CloudHSM should be always be used for any payment transactions'},
                    {"id":2,"option":"It probably doesn't matter as they both do the same thing"},
                    {"id":3,"option":'AWS CloudHSM does not support the processing, storage, and transmission of credit card data by a merchant or service provider, as it has not been validated as being compliant with Payment Card Industry (PCI) Data Security Standard (DSS); hence, you will need to use KMS'},
                    {"id":4,"option":'KMS is probably adequate unless additional protection is necessary for some applications and data that are subject to strict contractual or regulatory requirements for managing cryptographic keys, then HSM should be used'}],
        "correct":4,
        "paperId":1,
        "category":[2,4],
          "explanation":"Cloud Security best practices."
      },
      {
        "id":28,
        "question":'When reviewing the Auto Scaling events, it is noticed that an application is scaling up and down multiple times within the hour. What design change could you make to optimize cost while preserving elasticity?',
        "options":[ {"id":1,"option":'Add provisioned IOPS to the instances'},
                    {"id":2,"option":'Increase the instance type in the launch configuration'},
                    {"id":3,"option":'Increase the base number of Auto Scaling instances for the Auto Scaling group'},
                    {"id":4,"option":'Change the scale down CloudWatch metric to a higher threshold'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"Instance will keep in for long time and will protect the cost."
      },
      {
        "id":29,
        "question":'Your supervisor calls you wanting to know why an SWF workflow you created has not made any progress in the last three weeks. What is the most likely explanation for the workflow’s behavior?',
        "options":[ {"id":1,"option":'The last task has exceeded SWF’s 14-day maximum task execution time'},
                    {"id":2,"option":'SWF is awaiting human input from an activity task you assigned to your supervisor'},
                    {"id":3,"option":'The workflow has exceeded SWF’s 14-day maximum workflow execution time'},
                    {"id":4,"option":'SWF does not support the tasks you created for on-premises servers, so the workflow has entered a paused state'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"SWF task and workflow execution can last up to one year, and can include tasks to be performed by on-premises servers and humans."
      },
      {
        "id":30,
        "question":'Your AWS environment contains several on-demand EC2 instances dedicated to a project that has just been cancelled. Your supervisor does not want to incur charges for these on-demand instances, but also does not want to lose the data just yet because there is a chance the project may be revived in the next few days. What should you do to minimize charges for these instances in the meantime?',
        "options":[ {"id":1,"option":'Terminate the instances as soon as possible'},
                    {"id":2,"option":'Stop the instances as soon as possible'},
                    {"id":3,"option":'Contact AWS and explain the situation'},
                    {"id":4,"option":'Sell the instances on the AWS On-Demand Instance Marketplace. You can buy them back later if needed'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"You should not terminate an instance that you may need to place back into production in a few days. The best way to minimize charges is to stop the instances to avoid any data transfer charges that the instance might incur if left running."
      },
      {
        "id":31,
        "question":"While running an EC2 instance, you've been storing data on one of the instance's volumes. However, to save money, you shut down the instance for the weekend. The following week, after starting the instance, you notice that all your work has been lost and is no longer available on the EC2 instance. What might be the cause of this?",
        "options":[ {"id":1,"option":'The EC2 instance was using EBS backed root volumes, which are ephemeral and only live for the life of the instance'},
                    {"id":2,"option":'The EBS volume was not big enough to handle all of the processing data'},
                    {"id":3,"option":'The instance might have been compromised'},
                    {"id":4,"option":'The EC2 instance was using instance store volumes, which are ephemeral and only live for the life of the instance'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"Instance store is ephemeral storage. EBS volume should have been used for this requirement which will be available as you restart your instance."
      },
      {
        "id":32,
        "question":'Your supervisor asks you to create a highly available website which serves static content from EC2 instances. Which of the following is not a requirement to accomplish this goal?',
        "options":[ {"id":1,"option":'Multiple subnets'},
                    {"id":2,"option":'An SQS queue'},
                    {"id":3,"option":'Multiple Availability Zones'},
                    {"id":4,"option":'An auto scaling group to recover from EC2 instance failures'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"While an SQS queue can be an important part of a multi-step decoupled web application, it is not necessary to host a highly available static website on EC2. An auto scaling group configured to deploy EC2 instances in multiple subnets located in multiple availability zones allows an application to remain online despite an instance or AZ failure."
      },
      {
        "id":33,
        "question":'After setting up your first VPC peering connection between you and one of your clients, the client requests to be able to send traffic between instances in the peered VPCs using private IP addresses. What must you do to make this possible?',
        "options":[ {"id":1,"option":'Add his instance and your instance to the same placement group'},
                    {"id":2,"option":'Use IPSec tunneling'},
                    {"id":3,"option":'Set up a second VPC peering connection'},
                    {"id":4,"option":"Add a route to a route table that's associated with your VPC"}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"For sending/receiving traffic there should always be some routes available in routing tables for infrastructure."
      },
      {
        "id":34,
        "question":"You've been contacted by a client who is having connectivity issues on their Amazon Virtual Private Cloud and EC2 instances. After logging into the environment, you notice that the customer is using four instances that all belong to a subnet with an attached internet gateway. The instances also belong to the same security group. However, one of the instances is not able to send or receive traffic like the other three. After logging into the internal network and investigating, you find that the instance is working as expected and determine it is not an operating system issue. Which of the following is missing from the environment and is preventing that single instance from connecting?",
        "options":[ {"id":1,"option":'A proper route table configuration that sends traffic from the instance to the Internet through the internet gateway'},
                    {"id":2,"option":'The EC2 instance does not have a public IP address associated with it'},
                    {"id":3,"option":'The EC2 instance is running in an availability zone that does not support Internet gateways'},
                    {"id":4,"option":'The EC2 instance is not a member of the same Auto Scaling group/policy'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"Instance should have Public IP and internet gateway should be attached to the respective VPC for outside connectivity."
      },
      {
        "id":35,
        "question":'If your organization is concerned about storing sensitive data in the cloud, you should ?',
        "options":[ {"id":1,"option":'With AWS you do not need to worry about encryption'},
                    {"id":2,"option":'Enable EBS Encyption'},
                    {"id":3,"option":'Encrypt the file system on an EBS volume using Linux tools'},
                    {"id":4,"option":'Enable S3 Encryption'}],
        "correct":[2,3,4],
        "paperId":1,
        "category":[],
          "explanation":"You have to always choose whether you want encryption for your data or not."
      },
      {
        "id":36,
        "question":'Which of the following is not expected behavior from SQS and may indicate a problem with your application?',
        "options":[ {"id":1,"option":'Messages are retrieved from your SQS queue in a different order than they were created'},
                    {"id":2,"option":'Messages in JSON format fail to be created in the SQS queue'},
                    {"id":3,"option":'A message in your SQS queue is duplicated'},
                    {"id":4,"option":'A 500 KB message fails to be created in an SQS queue'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"SQS message can contain XML, JSON, text or unformatted data."
      },
      {
        "id":37,
        "question":'Which of the following CloudWatch metrics require a custom monitoring script to populate the metric?',
        "options":[ {"id":1,"option":'Swap usage'},
                    {"id":2,"option":'CPU utilization'},
                    {"id":3,"option":'CPU'},
                    {"id":4,"option":'Available Disk Space'}],
        "correct":[2,3],
        "paperId":1,
        "category":[],
          "explanation":"No available Cloudwatch metric for CPU. Only disk, memory and swap related metrics are available. "
      },
      {
        "id":38,
        "question":'Your applications in AWS need to authenticate against LDAP credentials that are in your on-premises data center. You need low latency between the AWS app authenticating and your on-premises network. How can you achieve this?',
        "options":[ {"id":1,"option":'If you don’t already have a secure tunnel, create a VPN between your on-premises data center and AWS. You can then spin up a secondary LDAP server that replicates from the on-premises LDAP server.'},
                    {"id":2,"option":'You don’t use LDAP to authenticate to your apps.'},
                    {"id":3,"option":'Create a Direct Connect tunnel which will decrease latency, increase bandwidth, and authenticate faster.'},
                    {"id":4,"option":'Create a new LDAP server and authenticate to it.'}],
        "correct":1,
        "paperId":1,
        "category":[],
          "explanation":"It is best practice to have a replica LDAP through VPN."
      },
      {
        "id":39,
        "question":'Why will the following CloudFormation template fail to deploy a stack?',
      //    "code":`{
      //        "AWSTemplateFormatVersion" : "2010-09-09",
      //
      //            "Parameters" : {
      //            "VPCId" : {
      //                "Type": "String",
      //                    "Description" : "Enter current VPC Id"
      //            },
      //            "SubnetId : {
      //            "Type": "String",
      //                "Description" : "Enter a subnet Id"
      //        }
      //    },
      //
      //    "Outputs" : {
      //        "InstanceId" : {
      //            "Value" : { "Ref" : "MyInstance" },
      //            "Description" : "Instance Id"
      //        }
      //    }
      //} `,
        "options":[ {"id":1,"option":'A “Conditions” section is mandatory but is not included'},
                    {"id":2,"option":'CloudFormation templates do not use a “Parameters” section'},
                    {"id":3,"option":'A template description is mandatory but is not included'},
                    {"id":4,"option":'A “Resources” section is mandatory but is not included'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"A “Resource” section is the main part of a CloudFormation template."
      },
      {
        "id":40,
        "question":'are some steps you can take to optimize costs on AWS?',
        "options":[ {"id":1,"option":'For RDS DB instances that consistently have 0 connections, take a snapshot of the instance and terminate the instance'},
                    {"id":2,"option":'Purchase reserved instances'},
                    {"id":3,"option":'Detach underutilized EBS volumes and take a snapshot of the EBS volume and then delete the EBS volume'},
                    {"id":4,"option":'AWS is already optimized in cost'}],
        "correct":[1,2,3],
        "paperId":1,
        "category":[2,4],
          "explanation":"AWS is not already optimized, you have to deploy infrastructure smartly."
      },
      {
        "id":41,
        "question":'You are a consultant tasked with migrating an on-premises application architecture to AWS. During your design process you have to give consideration to current on-premises security and determine which security attributes you are responsible for on AWS. Which of the following does AWS provide for you as part of the shared responsibility model?',
        "options":[ {"id":1,"option":'Physical network infrastructure'},
                    {"id":2,"option":'User access to the AWS environment'},
                    {"id":3,"option":'Virtualization infrastructure'},
                    {"id":4,"option":'Instance security'}],
        "correct":[1,3],
        "paperId":1,
        "category":[2,4],
          "explanation":"According to shared responsibility model strongly encounters physical hardware security."
      },
      {
        "id":42,
        "question":'An AWS VPC (Virtual Private Cloud) allows you to',
        "options":[ {"id":1,"option":'Not have to monitor security'},
                    {"id":2,"option":'Connect your cloud resources to your own encrypted IPSec VPN connections'},
                    {"id":3,"option":'Provision unlimited S3 resources'},
                    {"id":4,"option":'None of the above'}],
        "correct":2,
        "paperId":1,
        "category":[2,4],
          "explanation":"You can connect your own encrypted IPSec VPN connections to aws cloud resources in a VPC."
      },
      {
        "id":43,
        "question":'You attempt to create a new S3 bucket “Xyz-Bucket-12-US-East-1-Production-Envrionment-12.25.14” in the US-East-1 region and the bucket creation fails. Why?',
        "options":[ {"id":1,"option":'The bucket name uses the dash character (“-”)'},
                    {"id":2,"option":'The bucket name uses capital letters'},
                    {"id":3,"option":'The length of the bucket name (65 characters) is longer than the limit of 63 characters.'},
                    {"id":4,"option":'The bucket name uses the period character (“.”)'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"The S3 bucket name cannot have capital letters."
      },
      {
        "id":44,
        "question":'You have an EC2 instance deployed with an IAM role with write access permissions to an SQS queue. The instance is attempting to write a 512 KB message to an SQS queue. What will the result of this attempt be?',
        "options":[ {"id":1,"option":'It will succeed but be considered as 8 message requests because SQS measures message requests in 64 KB chunks.'},
                    {"id":2,"option":'It will fail because SQS requires the EC2 instance to use API keys with permissions to write to the queue.'},
                    {"id":3,"option":'It will succeed as a single message request.'},
                    {"id":4,"option":'It will fail because it is greater than the 256 KB limit for SQS messages.'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"SQS message size limit is 256KB but we are trying to write a 512KB message."
      },
      {
        "id":45,
        "question":'Which statement about DynamoDB is true?',
        "options":[ {"id":1,"option":'DynamoDB does not support conditional writes.'},
                    {"id":2,"option":'DynamoDB uses optimistic concurrency control.'},
                    {"id":3,"option":'DynamoDB is a relational database service.'},
                    {"id":4,"option":'None of the above'}],
        "correct":3,
        "paperId":1,
        "category":[],
          "explanation":"DynamoDB is a non-relational database."
      },
      {
        "id":46,
        "question":'By default, what event occurs if your CloudFormation receives an error during creation?',
        "options":[ {"id":1,"option":'DELETE_COMPLETE'},
                    {"id":2,"option":'CREATION_IN_PROGRESS'},
                    {"id":3,"option":'DELETE_IN_PROGRESS'},
                    {"id":4,"option":'ROLLBACK_IN_PROGRESS'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"Whenever a CloudFormation template fails, it starts roll_back."
      },
      {
        "id":47,
        "question":'An SWF workflow task or task execution can live up to ______ long',
        "options":[ {"id":1,"option":'1 year'},
                    {"id":2,"option":'3 days'},
                    {"id":3,"option":'14 days'},
                    {"id":4,"option":'24 hours'}],
        "correct":1,
        "paperId":1,
        "category":[2,4],
          "explanation":"An SWF workflow task have life span of one year."
      },
      {
        "id":48,
        "question":'You want 5 strongly consistent 1KB writes per second. How many units of throughput capacity do you need to provision?',
        "options":[ {"id":1,"option":'4'},
                    {"id":2,"option":'5'},
                    {"id":3,"option":'9'},
                    {"id":4,"option":'10'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"1 strongly consistent write is 1 KB in size * 5 Strongly consistent writes  needed = 5 units of  throughput capacity"
      },
      {
        "id":49,
        "question":'You created three S3 buckets – “mydomain.com”, “downloads.mydomain.com”, and “www.mydomain.com”. You uploaded your files, enabled static website hosting, specified both of the default documents under the “enable static website hosting” header, and set the “Make Public” permission for the objects in each of the three buckets. All that’s left for you to do is to create the Route 53 Aliases for the three buckets. You are going to have your end users test your websites by browsing to http://mydomain.com/error.html, http://downloads.mydomain.com/index.html, and http://www.mydomain.com. What problems will your testers encounter?',
        "options":[ {"id":1,"option":"http://mydomain.com/error.html will not work because you did not set a value for the error.html file"},
                    {"id":2,"option":'There will be no problems, all three sites should work.'},
                    {"id":3,"option":'http://downloads.mydomain.com/index.html will not work because the “downloads” prefix is not a supported prefix for S3 websites using Route 53 aliases'},
                    {"id":4,"option":'http://www.mydomain.com will not work because the URL does not include a file name at the end of it.'}],
        "correct":3,
        "paperId":1,
        "category":[],
          "explanation":"“downloads” prefix is not allowed for S3 websites"
      },
      {
        "id":50,
        "question":'You have reached your account limit for the number of CloudFormation stacks in a region. How do you increase your limit?',
        "options":[ {"id":1,"option":'You cannot increase your limit.'},
                    {"id":2,"option":'Contact AWS.'},
                    {"id":3,"option":'Use the console.'},
                    {"id":4,"option":'Make an API call.'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"The only way to increase the limit of CloudFormation stacks in a region is by contacting AWS."
      },
      {
        "id":51,
        "question":'A DynamoDB item is a collection of name and value attributes.',
        "options":[ {"id":1,"option":'True'},
                    {"id":2,"option":'False'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"An item is a collection of attributes. An attribute is a collection of name and value."
      },
      {
        "id":52,
        "question":'You need to announce an emergency downtime for a production AWS web application. This downtime notification will require different sets of instructions for different devices. All of the application users signed up to receive SNS notifications from the “mywebapp” topic when they began using the application and they are currently subscribed to this topic. What are appropriate ways for you to provide timely, device-specific instructions to end users when announcing this downtime?',
        "options":[ {"id":1,"option":'Send a single message, but customize the text in the SNS message field so that each device gets only the information that is appropriate for them'},
                    {"id":2,"option":'Create a different topic for each subscription type and send a message to SMS endpoints to one topic and a message to email endpoints to another topic'},
                    {"id":3,"option":'SNS is for automated notifications and you cannot send messages manually via SNS. The best option is to export the endpoints to a csv and send notifications to customers via your email client or SMS device.'},
                    {"id":4,"option":'Send multiple messages to the topic and ask users to ignore the messages that do not pertain to their device'}],
        "correct":1,
        "paperId":1,
        "category":[],
          "explanation":"Sending a single message to different platforms is the core feature of SNS."
      },
      {
        "id":53,
        "question":'What is the function of a conditional write?',
        "options":[ {"id":1,"option":'An S3 object will only be written to a bucket if it is less than 5 GB'},
                    {"id":2,"option":'A change to a DynamoDB attribute will only be written if it that attribute’s value has not changed since it was read'},
                    {"id":3,"option":'A change to a DynamoDB attribute will only be written if no other users are accessing the table'},
                    {"id":4,"option":'An S3 object will only be written to a bucket if is encrypted'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"That is what a conditional write is used for."
      },
      {
        "id":54,
        "question":'does not generally handle error codes with HTTP responses.',
        "options":[ {"id":1,"option":'True'},
                    {"id":4,"option":'False'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"S3 handles error codes with HTTP responses."
      },
      {
        "id":55,
        "question":'Which of the following is a default limit in S3?',
        "options":[ {"id":1,"option":'Buckets can have a maximum size of 5 TB'},
                    {"id":2,"option":'Accounts can have a maximum of 100 buckets'},
                    {"id":3,"option":'Objects can have a maximum size of 5 GB'},
                    {"id":4,"option":'Objects have no size limitation'}],
        "correct":2,
        "paperId":1,
        "category":[],
          "explanation":"An AWS account can have maximum of 100 buckets but can be increased by contacting Amazon."
      },
      {
        "id":56,
        "question":'You have items in your table that are 120KB in size and you want to have 10 strongly consistent reads per second. How many read capacity units would you need to provision?',
        "options":[ {"id":1,"option":'300'},
                    {"id":2,"option":'10'},
                    {"id":3,"option":'3'},
                    {"id":4,"option":'1'}],
        "correct":1,
        "paperId":1,
        "category":[],
          "explanation":"(120KB in size/4KB strongly consistent read size)*10 number of reads = 300 units of read capacity"
      },
      {
        "id":57,
        "question":'Your supervisor calls you wanting to know why she has not been receiving email notifications for AWS billing alerts. What do you suspect the problem might be and how can you find out?',
        "options":[ {"id":1,"option":'Billing alerts are not configured. Verify by viewing Billing Alerts in Account Preferences'},
                    {"id":2,"option":'The SNS queue is not AutoScaling properly. Verify by viewing Performance Statistics in SNS.'},
                    {"id":3,"option":'Your supervisor has not responded to the confirmation email sent from SNS when you added a subscription for her email address. Verify by viewing Subscriptions for the appropriate Topic in SNS'},
                    {"id":4,"option":'The SNS Subscription is not configured for Email notifications. Verify by viewing Subscriptions for the appropriate Topic in SNS'}],
        "correct":[1,3,4],
        "paperId":1,
        "category":[],
          "explanation":"Receiver has to confirm the subscription."
      },
      {
        "id":58,
        "question":'While working with the S3 API you receive the error: 403 forbidden. What is the most likely cause of this?',
        "options":[ {"id":1,"option":'BadDigest'},
                    {"id":2,"option":'BuckeAlreadyExist'},
                    {"id":3,"option":'NoSuchBucket'},
                    {"id":4,"option":'AccessDenied'}],
        "correct":4,
        "paperId":1,
        "category":[],
          "explanation":"403 is an HTTP code meaning AccessDenied"
      },
      {
        "id":59,
        "question":'What is the minimum size of an S3 object?',
        "options":[ {"id":1,"option":'1 Kb '},
                    {"id":2,"option":'1 Mb'},
                    {"id":3,"option":'0 byte'},
                    {"id":4,"option":'1 byte'}],
        "correct":3,
        "paperId":1,
        "category":[],
          "explanation":"S3 object limit is from 0 byte minimum to 5TB maximum."
      },
      {
        "id":60,
        "question":'You would like to set up a static website on S3 with the least possible effort. The URL of the website is unimportant to you. Which of the following steps are necessary?',
        "options":[ {"id":1,"option":'Upload an index document to your S3 bucket'},
                    {"id":2,"option":'Enable static website hosting in your S3 bucket properties'},
                    {"id":3,"option":'Select the “Make Public” permission for your bucket’s objects'},
                    {"id":4,"option":'Create an Alias record in Route 53'}],
        "correct":[1,2,3],
        "paperId":1,
        "category":[],
          "explanation":"Each bucket have a URL assigned by AWS. You can access the website without creating alias in Route 53."
      },
      {
        "id":61,
        "question":'By default, AWS allows you to have ____ tables per account, per region.',
        "options":[ {"id":1,"option":'256'},
                    {"id":2,"option":'128'},
                    {"id":3,"option":'224'},
                    {"id":4,"option":'64'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"Limit set by AWS by default."
      },
      {
        "id":61,
        "question":"You have software on an EC2 instance that needs to access both the private and public IP address of that instance. What's the best way for the software to get that information?",
        "options":[ {"id":1,"option":'Use the instance metadata for the private IP and user data for the public IP'},
                    {"id":2,"option":'Look it up in instance metadata'},
                    {"id":3,"option":'Make a call to the EC2 API'},
                    {"id":4,"option":'Look it up in user data'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"instance metadata contain all the information like private/public IP in its metadata."
      },
      {
        "id":63,
        "question":'You are creating several DynamoDB tables for a new project. While doing so, you receive the error message, “LimitExceededException.” You are well below the maximum number of tables per account and there is no read or write activity on the tables yet. Why have you received this error?',
        "options":[ {"id":1,"option":'You attempted to create global indexes at the same time you created the tables'},
                    {"id":2,"option":'You attempted to create local indexes at the same time you created the tables'},
                    {"id":3,"option":'You attempted to create more than one table with a secondary index at a time'},
                    {"id":4,"option":'You failed to pre-warm the tables'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"You cannot create more than one table with a secondary index at a time."
      },
      {
        "id":64,
        "question":'A global secondary index is an index with a hash and range key that can be different from those on the table.',
        "options":[ {"id":1,"option":'True'},
                    {"id":2,"option":'False'}],
        "correct":1,
        "paperId":2,
        "category":[2,4],
          "explanation":"A global secondary index can have different hash and range key from that on the table."
      },
      {
        "id":65,
        "question":'Which of the following statements is true about DynamoDB?',
        "options":[ {"id":1,"option":'Requests are eventually consistent unless otherwise specified.'},
                    {"id":2,"option":'Requests are strongly consistent'},
                    {"id":3,"option":'Tables do not contain primary keys.'},
                    {"id":4,"option":'None of above'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"DynamoDB supports both eventual consistency and strong consistency."
      },
      {
        "id":66,
        "question":'Which of the following statements about SQS is true?',
        "options":[ {"id":1,"option":'Messages will be delivered exactly once, and messages will be delivered in First in, First out order.'},
                    {"id":2,"option":'Messages will be delivered exactly once, and message delivery order is indeterminate.'},
                    {"id":3,"option":'Messages will be delivered one or more times, and messages will be delivered in First in, First out order.'},
                    {"id":4,"option":'Messages will be delivered one or more times, and message delivery order is indeterminate.'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"It is the main feature of SQS."
      },
      {
        "id":67,
        "question":'You receive a call from a potential client who explains that one of the many services they offer is a website running on a t2.micro EC2 instance where users can submit requests for customized e-cards to be sent to their friends and family. The e-card website administrator was on a cruise and was shocked when he returned to the office in mid-January to find hundreds of angry emails complaining that customers’ loved ones had not received their Christmas cards. He also had several emails from CloudWatch alerting him that the SQS queue for the e-card application had grown to over 500 messages on December 25th. You investigate and find that the problem was caused by a crashed EC2 instance which serves as an application server. What do you advise your client to do first?',
        "options":[ {"id":1,"option":'Send an apology to the customers notifying them that their cards will not be delivered.'},
                    {"id":2,"option":'Use an autoscaling group to create as many application servers as needed to process all of the Christmas card SQS messages.'},
                    {"id":3,"option":'Redeploy the application server as a larger instance type so that it can process the Christmas card SQS messages faster.'},
                    {"id":4,"option":'Reboot the application server immediately so that it begins processing the Christmas card SQS messages.'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"Christmas is gone. Current month is January and you cannot fix the situation. Just send an apology."
      },
      {
        "id":68,
        "question":'Which of the following AWS Services are offered at no cost?',
        "options":[ {"id":1,"option":'Auto Scaling'},
                    {"id":2,"option":'Simple Storage Service'},
                    {"id":3,"option":'Elastic Load Balancing'},
                    {"id":4,"option":'Amazon VPC'}],
        "correct":[1,4],
        "paperId":2,
        "category":[],
          "explanation":"Auto Scaling and VPC are offered at no cost from Amazon but the additional resources you deploy using these services include charges."
      },
      {
        "id":69,
        "question":'What is one key difference between an Amazon EBS-backed and an instance-store backed instance?',
        "options":[ {"id":1,"option":'Amazon EBS - backed instances can be stopped and restarted'},
                    {"id":2,"option":'Instance - store backed instances can be stopped and restarted.'},
                    {"id":3,"option":'Virtual Private Cloud requires EBS backed instances'},
                    {"id":4,"option":'Auto scaling requires using Amazon EBS - backed instances.'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"EBS volumes are not an ephemeral storage and their instances can be stopped and restarted."
      },
      {
        "id":70,
//        "question":"After having created a new Linux instance on Amazon EC2, and downloaded the .pem file (called LAfile.pem) you try and SSH into your IP address (52.2.222.22) using the following command.
//
//          ssh -i LAfile.pem ec2-user@52.2.222.22
//
//      However you receive the following error.
//
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//      @ WARNING: UNPROTECTED PRIVATE KEY FILE! @
//      @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//
//What is the most probable reason for this and how can you fix it?",
        "options":[ {"id":1,"option":'Your key file is not encrypted. You need to use the -u option for unencypted not the -i option as follows. "ssh -u LAfile.pem ec2-user@52.2.222.22"'},
                    {"id":2,"option":'Your key file does not have the correct permissions for you to run the command. You need to modify your pem file as follows "chmod 644 LAfile.pem"'},
                    {"id":3,"option":'Your key file must not be publicly viewable for SSH to work. You need to modify your pem file as follows "chmod 400 LAfile.pem"'},
                    {"id":4,"option":'You do not have root access on your terminal and need to use the sudo option for this to work as follows. "sudo ssh -i LAfile.pem ec2-user@52.2.222.22"'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"You should always use a protected key file with correct permissions."
      },
      {
        "id":71,
        "question":'When designing a cloud service based on AWS and you choose to use RRS on S3 instead of S3 standard storage type, what type of tradeoffs do you have to build your application around?',
        "options":[ {"id":1,"option":'RRS only has 99.99% availability'},
                    {"id":2,"option":'RRS only has 99.99% durability and you have to design automation around replacing lost objects'},
                    {"id":3,"option":'With RRS, in order to build automation buckets, you need to have specific ACLs added'},
                    {"id":4,"option":'With RRS you have to check-in and check-out jobs which can take up to 6 hours'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"Reduced Redundancy Storage gives you 99.99% durability and can be deleted by Amazon when there is storage required."
      },
      {
        "id":72,
        "question":"You have been told that you need to set up a bastion host by your manager in the cheapest, most secure way, and that you should be the only person that can access it via SSH. Which of the following setups would satisfy your manager's request?",
        "options":[ {"id":1,"option":'A large EC2 instance and a security group which only allows access on port 22'},
                    {"id":2,"option":'A large EC2 instance and a security group which only allows access on port 22 via your IP address'},
                    {"id":3,"option":'A small EC2 instance and a security group which only allows access on port 22 via your IP address'},
                    {"id":4,"option":'A small EC2 instance and a security group which only allows access on port 22'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"No need to use a large instance. Just spawn a small instance and allow access on port 22 via your IP address"
      },
      {
        "id":73,
        "question":'Your Company has an application that requires access to a NoSQL database. Your IT department has no desire to manage the NoSQL servers. Which Amazon service provides a fully-managed and highly available NoSQL service?',
        "options":[ {"id":1,"option":'Amazon RDS'},
                    {"id":2,"option":'Elastic MapReduce'},
                    {"id":3,"option":'Simple DB'},
                    {"id":4,"option":'Dynamo DB'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"DynamoDB is the NoSQL database provided by AWS."
      },
      {
        "id":74,
        "question":'You are deploying your first EC2 instance in AWS and are using the AWS console to do this. You have chosen your AMI and your instance type and have now come to the screen where you configure your instance details. One of the things that you need to decide is whether you want to auto-assign a public IP address or not. You assume that if you do not choose this option you will be able to assign an Elastic IP address later, which happens to be a correct assumption. Which of the below options best describes why an Elastic IP address would be preferable to a public IP address?',
        "options":[ {"id":1,"option":'An Elastic IP address cannot be accessed from the internet like a public IP address and hence is safer from a security standpoint.'},
                    {"id":2,"option":'An Elastic IP address is free, whilst you must pay for a public IP address.'},
                    {"id":3,"option":'With an Elastic IP address, you can mask the failure of an instance or software by rapidly remapping the address to another instance in your account.'},
                    {"id":4,"option":'You can have an unlimited amount of Elastic IP addresses, however public IP addresses are limited in number.'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"Elastic IP helps a lot in overcoming an instance failure. It can be easily assigned to another healthy instance."
      },
      {
        "id":75,
        "question":'By default is data in S3 encrypted?',
        "options":[ {"id":1,"option":'No, but it can be when the right APIs are called for SSE'},
                    {"id":2,"option":'Yes, S3 always encrypts data for security'},
                    {"id":3,"option":'No, it must be encrypted before upload'},
                    {"id":4,"option":'Yes, but only in government cloud data centers'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"Data is securely saved in S3 buckets by default."
      },
      {
        "id":76,
        "question":'EC2 instances are launched from Amazon Machine Images (AMIs). A given public AMI:',
        "options":[ {"id":1,"option":'can only be used to launch EC2 instances in the same AWS availability zone as the AMI is stored'},
                    {"id":2,"option":'can only be used to launch EC2 instances in the same AWS region as the AMI is stored'},
                    {"id":3,"option":'can be used to launch EC2 instances in any AWS region'},
                    {"id":4,"option":'can only be used to launch EC2 instances in the same country as the AMI is stored'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"Amazon Cloud is divided into regions and a region is further divided into availability zones. AMIs belonging to a specific region can used for instance spawning in same region."
      },
      {
        "id":77,
        "question":"You've been contacted by a client who is having connectivity issues on their Amazon Virtual Private Cloud and EC2 instances. After logging into the environment, you notice that the customer is using four instances that all belong to a subnet with an attached internet gateway. The instances also belong to the same security group. However, one of the instances is not able to send or receive traffic like the other three. After logging into the internal network and investigating, you find that the instance is working as expected and determine it is not an operating system issue. Which of the following is missing from the environment and is preventing that single instance from connecting?",
        "options":[ {"id":1,"option":'The EC2 instance is not a member of the same Auto Scaling group/policy'},
                    {"id":2,"option":'The EC2 instance is running in an availability zone that does not support Internet gateways'},
                    {"id":3,"option":'The EC2 instance does not have a public IP address associated with it'},
                    {"id":4,"option":'A proper route table configuration that sends traffic from the instance to the Internet through the internet gateway'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"An internet gateway and a public IP must be attached to the instance inside a VPC for internet access."
      },
      {
        "id":78,
        "question":"You've been tasked with building out a duplicate environment in another region for disaster recovery purposes. Part of your environment relies on EC2 instances with preconfigured software. What steps would you take to configure the instances in another region?",
        "options":[ {"id":1,"option":'Create an AMI of the EC2 instance'},
                    {"id":2,"option":'Create an AMI of the EC2 instance and copy the AMI to the desired region'},
                    {"id":3,"option":'Make the EC2 instance shareable among other regions through IAM permissions'},
                    {"id":4,"option":'None of above'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"You do need to create AMI and copy it to desired regions for making instance replicas."
      },
      {
        "id":79,
        "question":'Your Company requires that all the data on your EBS-backed EC2 volumes be encrypted. How would you go about doing this?',
        "options":[ {"id":1,"option":'You cannot enable EBS encryption on a specific volume'},
                    {"id":2,"option":'AWS allows you to encrypt the file system on an EBS volume on EBS volume setup'},
                    {"id":3,"option":'Encryption can be done on the OS layer of the EBS volume'},
                    {"id":4,"option":'None of the above'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"AWS allows encryption of an EBS-backed EC2 volume."
      },
      {
        "id":80,
        "question":'You are using IOT sensors to monitor the movement of a group of hikers on a three day trek and send the information into a Kinesis Stream. They each have a sensor in their shoe and you know for certain that there is no problem with mobile coverage so all the data is getting back to the stream. You have used default settings for the stream. At the end of the third day the data is sent to an S3 bucket. When you go to interpret the data in S3 there is only data for the last day and nothing for the first 2 days. Which of the following is the most probable cause of this?',
        "options":[ {"id":1,"option":'A sensor probably stopped working on the second day. If one sensor fails, no data is sent to the stream until that sensor is fixed'},
                    {"id":2,"option":"You cannot send Kinesis data to the same bucket on consecutive days if you do not have versioning enabled on the bucket. If you don't have versioning enabled you would need to define 3 different buckets or else the data is overwritten each day"},
                    {"id":3,"option":'Temporary loss of mobile coverage; although mobile coverage was good in the area, even temporary loss of data will stop the streaming'},
                    {"id":4,"option":'Data records are only accessible for a default of 24 hours from the time they are added to a stream'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"Limit set by AWS."
      },
      {
        "id":81,
        "question":'Which of the following will occur when an EC2 instance in a VPC with an associated Elastic IP is stopped and started?',
        "options":[ {"id":1,"option":'The Elastic IP will be dissociated from the instance'},
                    {"id":2,"option":'The underlying host for the instance can be changed'},
                    {"id":3,"option":'The ENI (Elastic Network Interface) is detached'},
                    {"id":4,"option":'All data on instance-store devices will be lost'}],
        "correct":[1,3],
        "paperId":2,
        "category":[],
          "explanation":"Elastic will be free for its assignment to another instance."
      },
      {
        "id":82,
        "question":'Amazon Glacier is designed for:',
        "options":[ {"id":1,"option":'Cached session data'},
                    {"id":2,"option":'Data archives'},
                    {"id":3,"option":'Infrequently accessed data'},
                    {"id":4,"option":'Active database storage'}],
        "correct":[2,3],
        "paperId":2,
        "category":[],
          "explanation":"Glacier is low cost storage. You can store your infrequent access and archived data on glacier on a very low-price."
      },
      {
        "id":83,
        "question":"You are consulting for a finance company that has specific backup and archiving policies. This company's RTO for all financial documents created in the past 6 months is 1 hour. The second requirement is to configure a setup that allows for all documents that are 6 months or older to be sent automatically for archiving in a lower-cost but highly durable archive environment. Given that the company is using the storage gateway, gateway-stored configuration, which of the following would be the best setup to reach the objectives?",
        "options":[ {"id":1,"option":'Enable an S3 lifecycle policy to integrate into Amazon EBS for a good backup solution'},
                    {"id":2,"option":'Enable an S3 lifecycle policy to immediately send all objects added to the bucket to Glacier'},
                    {"id":3,"option":'Enable S3 versioning with a lifecycle policy that sends objects older than 6 months to Amazon Glacier'},
                    {"id":4,"option":'Enable versioning on the S3 connected bucket to the gateway storage configuration'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"Older version will be sent out to Glacier and will incur a low cost."
      },
      {
        "id":84,
        "question":'One of your more important clients is a Telecom business who needs to process some real-time data in a distributed manner. They suggest to you that they think they should use either Amazon SQS or Amazon Kinesis to achieve this and they want you to tell them what would be the difference between the two. After some research you decide that they should use Kinesis and are trying to put together some reasons for this. One of the below statements is INCORRECT, regarding this. Which one?',
        "options":[ {"id":1,"option":'Amazon SQS, because of its distributed nature does not, despite what the name suggests, guarantee FIFO(First in First Out). Kinesis can guarantee FIFO'},
                    {"id":2,"option":'Kinesis has the ability for multiple applications to consume the same stream concurrently'},
                    {"id":3,"option":'Kinesis has the ability to consume data records in the same order a few hours later'},
                    {"id":4,"option":'Kinesis cannot route related data records to the same record processor (as in streaming MapReduce).'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"Telecom sector business nature will need the FIFO order to be maintained while getting real-time data in a distributed manner."
      },
      {
        "id":85,
        "question":"You're building out an application on AWS that is running within a single region. However, you're designing with disaster recovery in mind. Your intention is to build the application so that if the current region becomes unavailable, you can failover to another region. Part of your application relies heavily on pre-built AMIs. In order to share those AMIs with the region you're using as a backup, what process would you take?",
        "options":[ {"id":1,"option":'Modify the image permissions to share the AMI with another account, then set the default region to the backup region'},
                    {"id":2,"option":'Modify the image permissions to share to the designated backup region'},
                    {"id":3,"option":'Copy the AMI from the current region to another region, modify the Auto Scaling groups in the backup region to use the new AMI ID in the backup region'},
                    {"id":4,"option":"Nothing, because all AMI's are available in any region as long as it is created within the same account"}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"You have to copy the AMI to your desired region and boot instance from it."
      },
      {
        "id":86,
        "question":'Which region ID is the US Standard region?',
        "options":[ {"id":1,"option":'eu-east-1'},
                    {"id":2,"option":'us-east-1'},
                    {"id":3,"option":'us-east-1a'},
                    {"id":4,"option":'us-east-2'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"us-east-1 is the US standard region."
      },
      {
        "id":87,
        "question":'The AMI ID used in an Auto Scaling policy is configured in the _______.',
        "options":[ {"id":1,"option":'Auto Scaling group'},
                    {"id":2,"option":'Group policy'},
                    {"id":3,"option":'Launch configuration'},
                    {"id":4,"option":'Auto Scaling policy'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"The auto scaling group includes the AMI and block device mapping."
      },
      {
        "id":88,
        "question":'A VPC subnet can have multiple route tables.',
        "options":[ {"id":1,"option":'True'},
                    {"id":2,"option":'False'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"A VPC subnet can only have a single route table."
      },
      {
        "id":89,
        "question":'Your supervisor calls you wanting to know why an SWF workflow you created has not made any progress in the last three weeks. What is the most likely explanation for the workflow’s behavior?',
        "options":[ {"id":1,"option":'SWF does not support the tasks you created for on-premises servers, so the workflow has entered a paused state'},
                    {"id":2,"option":'SWF is awaiting human input from an activity task you assigned to your supervisor'},
                    {"id":3,"option":'The workflow has exceeded SWF’s 14-day maximum workflow execution time'},
                    {"id":4,"option":'The last task has exceeded SWF’s 14-day maximum task execution time'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"Worker needs to update the Decider that work is having what status in an SWF environment."
      },
      {
        "id":90,
        "question":'Being a good solutions architect means you will always plan for failures in your AWS cloud environment.',
        "options":[ {"id":1,"option":'True'},
                    {"id":2,"option":'False'}],
        "correct":1,
        "paperId":2,
        "category":[2,4],
          "explanation":"While architecting an infrastructure, thinking about failures will only lead you to design a good architecture."
      },
      {
        "id":91,
        "question":'You have multiple instances behind private and public subnets. None of the instances have an EIP assigned to them. How can you connect them to the internet to download system updates? ',
        "options":[ {"id":1,"option":'Connect to a VPN'},
                    {"id":2,"option":'Assign EIP to each instance'},
                    {"id":3,"option":'Create a NAT instance'},
                    {"id":4,"option":'Both b and c'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"Create a NAT instance and let the instances traffic pass through this NAT instance. Assign EIP to this NAT instance. Instances will now have internet access through NAT and will not be exposed to outside world."
      },
      {
        "id":92,
        "question":'Which of the following AWS services allow you access to the underlying operating system?',
        "options":[ {"id":1,"option":'Amazon RDS'},
                    {"id":2,"option":'Amazon EMR'},
                    {"id":3,"option":'Amazon Elastic Beanstalk'},
                    {"id":4,"option":'Amazon S3'}],
        "correct":[2,3],
        "paperId":2,
        "category":[],
          "explanation":"AWS provides the root or system privileges only for a limited set of services."
      },
      {
        "id":93,
        "question":'Amazon Auto Scaling is not meant to handle instant load spikes but is built to grow with a gradual increase in usage over a short time period.',
        "options":[ {"id":1,"option":'True'},
                    {"id":2,"option":'False'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"Auto Scaling can handle instant load spikes."
      },
      {
        "id":94,
        "question":'You are asked to review a plan that your company has made to create a new application that makes use of SQS, EC2, Auto Scaling and CloudWatch. Which of the following action items should you advise your company not to implement?',
        "options":[ {"id":1,"option":'Utilize an IAM role to grant EC2 instances permission to modify the SQS queue'},
                    {"id":2,"option":'Utilize short polling with a wait time of 20 seconds to reduce the number of empty responses from the SQS queue'},
                    {"id":3,"option":'Utilize AutoScaling to deploy new EC2 instances if the SQS queue grows too large'},
                    {"id":4,"option":'Utilize CloudWatch alarms to alert when the number of messages in the SQS queue grows too large'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"Long polling will reduce the number of empty responses but not short polling."
      },
      {
        "id":95,
        "question":'Your EC2 instances are configured to run behind an Amazon VPC. You have assigned two web serves instances to an Elastic Load Balancer. However, the instances and the ELB are not reachable via URL to the elastic load balancer serving the web app data from the EC2 instances. How might you resolve the issue so that your instances are serving the web app data to the public Internet?',
        "options":[ {"id":1,"option":'Attach an Internet gateway to the VPC and route it to the subnet'},
                    {"id":2,"option":'Add an elastic IP address to the instance'},
                    {"id":3,"option":'Use Amazon Elastic Load Balancer to serve requests to your instances located in the internal subnet'},
                    {"id":4,"option":'None of above'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"Routing the internet gateway to the subnet will make ELB and instances reachable."
      },
      {
        "id":96,
        "question":'Ten students have just been employed by your company for one week and it is your task to provide them with access to AWS, through IAM. Your supervisor has come to you and said that he wants to be able to track these students as a group, rather than individually. Because of this, he has requested for them to all have the same login ID but completely different passwords. Which of the following is the best way to achieve this?',
        "options":[ {"id":1,"option":"It isn't possible to have the same login ID for multiple IAM users of the same account"},
                    {"id":2,"option":'Use Multi Factor Authentication to verify each user and they will all be able to use the same login'},
                    {"id":3,"option":'Create various groups and add each user with the same login ID to different groups. The user can login with their own group ID'},
                    {"id":4,"option":'Create a separate login ID, but give each IAM user the same alias so that each one can login with their alias'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"Because supervisor wants to track students as a group and do not want to track them individually. Thus, there is no meaning in creating separate login IDs."
      },
      {
        "id":97,
        "question":'What is the maximum size of a general SSD EBS volume?',
        "options":[ {"id":1,"option":'2TB'},
                    {"id":2,"option":'16TB'},
                    {"id":3,"option":'4GB'},
                    {"id":4,"option":'16TB'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"Limit set by AWS."
      },
      {
        "id":98,
        "question":'You have created a VPC that has just one subnet with an internet gateway attached. Which of the following is true?',
        "options":[ {"id":1,"option":'It can connect '},
                    {"id":2,"option":'It does not need a NAT instance or an EIP to communicate with the internet'},
                    {"id":3,"option":'It needs an EIP or public ip assigned to it in order to connect to the internet and send data in or out'},
                    {"id":4,"option":'None of the above'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"Public Ip and an internet gateway is necessary for an instance to connect to internet."
      },
      {
        "id":99,
        "question":'What is the hourly rate to run a VPC?',
        "options":[ {"id":1,"option":'Free'},
                    {"id":2,"option":'.05/hour'},
                    {"id":3,"option":'.002/hour'},
                    {"id":4,"option":'.01/hour'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"Using VPC is free but services used under VPC incur charges."
      },
      {
        "id":100,
        "question":'To connect your remote office to your VPC for internal network access, what would you need to use?',
        "options":[ {"id":1,"option":'VPN'},
                    {"id":2,"option":'Server'},
                    {"id":3,"option":'Elastic IP Address'},
                    {"id":4,"option":'None of above'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"VPN is required to connect your on-premises servers with your VPC."
      },
      {
        "id":101,
        "question":'Elastic Load Balancing uses what technologies?',
        "options":[ {"id":1,"option":'EC2'},
                    {"id":2,"option":'DNS'},
                    {"id":3,"option":'Route53'},
                    {"id":4,"option":'RDS'}],
        "correct":[2,3],
        "paperId":2,
        "category":[],
          "explanation":"There is nothing to do with EC2 and RDS in case of a load balancer."
      },
      {
        "id":102,
        "question":'When can you add a secondary index to a table?',
        "options":[ {"id":1,"option":'Anytime as long as it is done with the AWS console'},
                    {"id":2,"option":'Anytime but a request to AWS is required so they do it for you'},
                    {"id":3,"option":'Only at table creation time'},
                    {"id":4,"option":'Anytime if it is a global index'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"A global secondary index can be created at any time but a local secondary index needs only to be created at table creation time."
      },
      {
        "id":103,
        "question":'What is the primary difference between a global secondary index and a local secondary index?',
        "options":[ {"id":1,"option":'A local secondary index has the same partition key as the primary key and the global secondary index has a different partition and sort key'},
                    {"id":2,"option":'The global secondary index is not region specific'},
                    {"id":3,"option":'A global secondary index has the same partition key as the primary key and the local secondary index has a different partition and sort key'},
                    {"id":4,"option":'There are no differences'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"Global index can have different partition key than the primary key."
      },
      {
        "id":104,
        "question":'You are having trouble maintaining session states on some of your applications that are using an Elastic Load Balancer (ELB). As well as that there does not seem to be an even distribution of sessions across your ELB. To overcome this problem which of the following is the recommended method by AWS to try and rectify the issues that you are having?',
        "options":[ {"id":1,"option":"a)	Use a special cookie to track the instance for each request to each listener. When the load balancer receives a request, it will then check to see if this cookie is present in the request."},
                    {"id":2,"option":"b)	If your application does not have its own session cookie, then you can configure Elastic Load Balancing to create a session cookie by specifying your own stickiness duration."},
                    {"id":3,"option":"c)	Use ElastiCache, which is a web service that makes it easy to set up, manage, and scale a distributed in-memory cache environment in the cloud."},
                    {"id":4,"option":"d)	Use the sticky session feature (also known as session affinity), which enables the load balancer to bind a user's session to a specific instance. This ensures that all requests from the user during the session are sent to the same instance."}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"AWS prohibits using stickiness for session maintaining."
      },
      {
        "id":105,
        "question":'A friend wants you to set up a small BitTorrent storage area for him on Amazon S3. You tell him it is highly unlikely that AWS would allow such a thing in their infrastructure. However you decide to investigate. Which of the following statements best describes using BitTorrent with Amazon S3?',
        "options":[ {"id":1,"option":'You can use the BitTorrent protocol but only for objects that are less than 5 GB in size.'},
                    {"id":2,"option":'You can use the BitTorrent protocol but you need to ask AWS for specific permissions first.'},
                    {"id":3,"option":'Amazon S3 does not support the BitTorrent protocol because it is used for pirated software.'},
                    {"id":4,"option":'You can use the BitTorrent protocol but only for objects that are less than 100 GB in size.'}],
        "correct":1,
        "paperId":2,
        "category":[],
        "explanation":"BitTorrent is an open, peer-to-peer protocol for distributing files. You can use the BitTorrent protocol to retrieve any publicly-accessible object in Amazon S3. Amazon S3 supports the BitTorrent protocol so that developers can save costs when distributing content at high scale. Amazon S3 is useful for simple, reliable storage of any data. The default distribution mechanism for Amazon S3 data is via client/server download. In client/server distribution, the entire object is transferred point-to-point from Amazon S3 to every authorized user who requests that object. While client/server delivery is appropriate for a wide variety of use cases, it is not optimal for everybody. Specifically, the costs of client/server distribution increase linearly as the number of users downloading objects increases. This can make it expensive to distribute popular objects.         BitTorrent addresses this problem by recruiting the very clients that are downloading the object as distributors themselves: Each client downloads some pieces of the object from Amazon S3 and some from other clients, while simultaneously uploading pieces of the same object to other interested peers. The benefit for publishers is that for large, popular files the amount of data actually supplied by Amazon S3 can be substantially lower than what it would have been serving the same clients via client/server download. Less data transferred means lower costs for the publisher of the object."

      },
      {
        "id":106,
        "question":'A major finance organization has engaged your company to set up a large data mining application. Using AWS you decide the best service for this is Amazon Elastic MapReduce(EMR) which you know uses Hadoop. Which of the following statements best describes Hadoop?',
        "options":[ {"id":1,"option":'Hadoop is 3rd Party software which can be installed using AMI'},
                    {"id":2,"option":'Hadoop is an open source JavaScript framework'},
                    {"id":3,"option":'Hadoop is an open source Php framework'},
                    {"id":4,"option":'Hadoop is an open source Java software framework'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"Amazon EMR uses Apache Hadoop as its distributed data processing engine. Hadoop is an open source, Java software framework that supports data-intensive distributed applications running on large clusters of commodity hardware. Hadoop implements a programming model named “MapReduce,” where the data is divided into many small fragments of work, each of which may be executed on any node in the cluster.This framework has been widely used by developers, enterprises and startups and has proven to be a reliable software platform for processing up to petabytes of data on clusters of thousands of commodity machines."
      },
      {
        "id":107,
        "question":'In Amazon RDS, security groups are ideally used to:',
        "options":[ {"id":1,"option":'Create, describe, modify, and delete DB instances'},
                    {"id":2,"option":'Launch Amazon RDS instances in a subnet'},
                    {"id":3,"option":'Control what IP addresses or EC2 instances can connect to your databases on a DB instance'},
                    {"id":4,"option":'Define maintenance period for database engines'}],
        "correct":3,
        "paperId":2,
        "category":[],
          "explanation":"In Amazon RDS, security groups are used to control what IP addresses or EC2 instances can connect to your databases on a DB instance. When you first create a DB instance, its firewall prevents any database access except through rules specified by an associated security group"
      },
      {
        "id":108,
        "question":'You are signed in as root user on your account but there is an Amazon S3 bucket under your account that you cannot access. What is a possible reason for this?',
        "options":[ {"id":1,"option":'The S3 bucket is full'},
                    {"id":2,"option":'The S3 bucket has reached the maximum number of objects allowed.'},
                    {"id":3,"option":'You are in the wrong availability zone'},
                    {"id":4,"option":"An IAM user assigned a bucket policy to an Amazon S3 bucket and didn't specify the root user as a principal"}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"With IAM, you can centrally manage users, security credentials such as access keys, and permissions that control which AWS resources users can access."
      },
      {
        "id":109,
        "question":'What is the time period with which metric data is sent to CloudWatch when detailed monitoring is enabled on an Amazon EC2 instance?',
        "options":[ {"id":1,"option":'5 minutes'},
                    {"id":2,"option":'30 seconds'},
                    {"id":3,"option":'15 minutes'},
                    {"id":4,"option":'1 minute'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"By default, Amazon EC2 metric data is automatically sent to CloudWatch in 5-minute periods. However, you can, enable detailed monitoring on an Amazon EC2 instance, which sends data to CloudWatch in 1-minute periods"
      },
      {
        "id":110,
        "question":'You have been using T2 instances as your CPU requirements have not been that intensive. However you now start to think about larger instance types and start looking at M1 and M3 instances. You are a little confused as to the differences between them as they both seem to have the same ratio of CPU and memory. Which statement below is incorrect as to why you would use one over the other?',
        "options":[ {"id":1,"option":'M3 instances also offer SSD-based instance storage that delivers higher I/O performance.'},
                    {"id":2,"option":'M3 instances are less expensive than M1 instances.'},
                    {"id":3,"option":'M3 instances provide better, more consistent performance that M1 instances for most use-cases.'},
                    {"id":4,"option":'M3 instances are configured with more swap memory than M1 instances.'}],
        "correct":4,
        "paperId":2,
        "category":[]
//          "explanation":"Amazon EC2 allows you to set up and configure everything about your instances from your operating system up to your applications. An Amazon Machine Image (AMI) is simply a packaged-up environment that includes all the necessary bits to set up and boot your instance. M1 and M3 Standard instances have the same ratio of CPU and memory, some reasons below as to why you would use one over the other.        "
//      •	M3 instances provide better, more consistent performance that M1 instances for most use-cases.
//•	M3 instances also offer SSD-based instance storage that delivers higher I/O performance.
//•	M3 instances are also less expensive than M1 instances. Due to these reasons, we recommend M3 for applications that require general purpose instances with a balance of compute, memory, and network resources.
//•	However, if you need more disk storage than what is provided in M3 instances, you may still find M1 instances useful for running your applications.

  },
      {
        "id":111,
        "question":'A user has attached 1 EBS volume to a VPC instance. The user wants to achieve the best fault tolerance of data possible. Which of the below mentioned options can help achieve fault tolerance?',
        "options":[ {"id":1,"option":'Use the EBS volume as a root device.'},
                    {"id":2,"option":'Attach one more volume with RAID 0 configuration.'},
                    {"id":3,"option":'Connect multiple volumes and stripe them with RAID 6 configuration.'},
                    {"id":4,"option":'Attach one more volume with RAID 1 configuration.'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"The user can join multiple provisioned IOPS volumes together in a RAID 1 configuration to achieve better fault tolerance. RAID 1 does not provide a write performance improvement; it requires more bandwidth than non-RAID configurations since the data is written simultaneously to multiple volumes."
      },
      {
        "id":112,
        "question":"You need to set up a security certificate for a client's e-commerce website as it will use the HTTPS protocol. Which of the below AWS services do you need to access to manage your SSL server certificate?",
        "options":[ {"id":1,"option":'Amazon Route 53'},
                    {"id":2,"option":'AWS CloudFormation'},
                    {"id":3,"option":'AWS Directory Service'},
                    {"id":4,"option":'AWS Identity & Access Management'}],
        "correct":4,
        "paperId":2,
        "category":[],
          "explanation":"AWS Identity and Access Management (IAM) is a web service that enables Amazon Web Services (AWS) customers to manage users and user permissions in AWS.          All your SSL server certificates are managed by AWS Identity and Access management (IAM)."
      },
      {
        "id":113,
        "question":'Doug has created a VPC with CIDR 10.201.0.0/16 in his AWS account. In this VPC he has created a public subnet with CIDR block 10.201.31.0/24. While launching a new EC2 from the console, he is not able to assign the private IP address 10.201.31.6 to this instance. Which is the most likely reason for this issue?',
        "options":[ {"id":1,"option":'Private address IP 10.201.31.6 is currently assigned to another interface.'},
                    {"id":2,"option":"Private IP address 10.201.31.6 is not part of the associated subnet's IP address range."},
                    {"id":3,"option":'Private IP address 10.201.31.6 is reserved by Amazon for IP networking purposes.'},
                    {"id":4,"option":'Private IP address 10.201.31.6 is blocked via ACLs in Amazon infrastructure as a part of platform security.'}],
        "correct":1,
        "paperId":2,
        "category":[]
//          "explanation":"In Amazon VPC, you can assign any Private IP address to your instance as long as it is:
//•	Part of the associated subnet's IP address range
//•	Not reserved by Amazon for IP networking purposes
//•	Not currently assigned to another interface
//      "
      },
      {
        "id":114,
        "question":'To protect S3 data from both accidental deletion and accidental overwriting, you should:',
        "options":[ {"id":1,"option":'enable S3 versioning on the bucket'},
                    {"id":2,"option":'access S3 data using only signed URLs'},
                    {"id":3,"option":'disable S3 delete using an IAM bucket policy'},
                    {"id":4,"option":'enable S3 Reduced Redundancy Storage'},
                    {"id":5,"option":'enable Multi-Factor Authentication (MFA) protected access'}],
        "correct":1,
        "paperId":2,
        "category":[],
          "explanation":"As the name suggests, S3 versioning means that all versions of a file are kept and retrievable at a later date (by making a request to the bucket, using the object ID and also the version number). The only charge for having this enabled is from the fact that you will incur more storage. When an object is deleted, it will still be accessible just not visible."
      },
      {
        "id":115,
        "question":'Which is an operational process performed by AWS for data security?',
        "options":[ {"id":1,"option":'AES-256 encryption of data stored on any shared storage device'},
                    {"id":2,"option":'Decommissioning of storage devices using industry-standard practices'},
                    {"id":3,"option":'Background virus scans of EBS volumes and EBS snapshots'},
                    {"id":4,"option":'Replication of data across multiple AWS Regions E. Secure wiping of EBS    data when an EBS volume is un-mounted'}],
        "correct":2,
        "paperId":2,
        "category":[],
          "explanation":"Clearly there is no way you could do this, so AWS take care."
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      },
      {
        "id":1,
        "question":'',
        "options":[ {"id":1,"option":'option1'},
                    {"id":2,"option":'option2'},
                    {"id":3,"option":'option3'},
                    {"id":4,"option":'option4'}],
        "correct":2,
        "paperId":2,
        "category":[2,4],
          "explanation":""
      }
    ]
  })
  .factory('backButtonForHome', ['$state', '$ionicPlatform', '$ionicHistory', '$timeout', function ($state, $ionicPlatform, $ionicHistory) {

        var obj = {};
        obj.backcallfun = function () {
            $ionicPlatform.registerBackButtonAction(function () {
                if ($state.current.name == "home") {
                    var action = confirm("Do you want to Exit?");
                    if (action) {
                        navigator.app.exitApp();
                    }//no else here just if

                } else {
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    //$state.lastView=$ionicHistory.backView();
                    //console.log($state.lastView.stateName);
                    $state.go('home');

                    //go to home page
                }
            }, 100);//registerBackButton
        }//backcallfun
        return obj;
    }])
    .factory('backButtonForQuestions', ['$state', '$ionicPlatform', '$ionicHistory', '$timeout', function ($state, $ionicPlatform, $ionicHistory, $timeout,$scope) {

        var obj = {}
        obj.backcallfun = function () {
            $ionicPlatform.registerBackButtonAction(function () {
                if ($state.current.name == "question") {
                    var action = confirm("This will end the quiz without saving. Do you want to continue?");
                    if (action) {
                        console.log(action);
                        $state.go("home");
                    }//no else here just if

                } else {
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    //$state.lastView=$ionicHistory.backView();
                    //console.log($state.lastView.stateName);
                    $state.go('home');

                    //go to home page
                }
            }, 100);//registerBackButton
        }//backcallfun
        return obj;
    }])

  .service('currentQuiz', function (questions) {
      //
        this.filteredQuestions;
        this.type;
        this.paper;

      this.selectQuestions=function() {
          console.log(this.type);
          console.log(this.paper);
          var paper=this.paper;
          if(this.type=="paper")
          {
             this.filteredQuestions= angular.copy(_.filter(questions, {'paperId': this.paper}));
              console.log(this.filteredQuestions);
              console.log("filtered by paper");
              console.log(this.filteredQuestions.length);
          }
          //if(this.type="topic")
          else{
              this.filteredQuestions= angular.copy(_.filter(questions,function(ques) {
                  console.log(paper);
                  return _.indexOf(ques.category,paper)!=-1;
              }));
           console.log("filtered by topic");
          }
          console.log(questions);
      }

})
    .service('quizHistory', function () {
       //for save quiz history
       this.history=[];

    })
    .service('saveParams', function () {
        //for save quiz history
        this.params={};
        this.types={};

    })
